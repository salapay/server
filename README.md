# SalaPay - assignment source code repository

## Introduction

Thank you for taking the time to review my assignment.\
In the last five years I was mainly developing JavaEE/Spring/OSGI-Equniox applications, although I was also develop 
node.js/Javascript/Typescript applications to some extent. Therefore I decided to fulfill your assignment with a stack 
of Java+Spring(+ReactJS) technologies where I can give you the best overview of my technology, framework and coding skills
and knowledge.

## Try it out with the live test instance
 * Swagger documentation (hosted): https://salapay.wagnergerald.com/swagger/index.html#/user/createUser
   * **please use a private tab in your browser to avoid any session issues** 
   * username: docs
   * password: docs
   * The database is pre-filled with some data, e.g. the users with the following id's already exits: 5,9,13
* How to use it:
   * The endpoints are ordered so you can step through all endpoints and try them with the test data provided like in the gif below
     * an exception is the getTransactionState, where you have to insert an valid transaction UUDID manually
   * ![Swagger-HowTo.gif](./docs/Swagger-HowTo.gif)
 * View the database with the H2 Console: https://salapay.wagnergerald.com/h2-console
    * Use the following data:
      * connection: jdbc:h2:mem:testdb
      * user: sa
      * password left blank
      
       ![h2-connection](./docs/H2Connection.PNG)
   
* If you want to use your own client, please use the following credentials:
 * Basic authentication with:
   * user: api
   * password: api
 * The bas url for the API is: https://salapay.wagnergerald.com/api/1.0.0/   

## Architecture & design considerations

Backend:
 * Backend REST Services are provided via the Java+Spring Boot stack
   * Service endpoints will put the payment requests in a JMS Queue (ActiveMQ)
   * Multiple queue consumers will process all events with multiple events depending on their unique id in order
   * Performance improvement could be adding multiple workers, that would have an impact on the queue order  
   * Persistent data storage via JPA and suitable databases (H2, MySQL, PostgreSQL, ...)
   * Endpoints are secured using a [spring security configuration](https://gitlab.com/salapay/server/-/tree/master/salapay-server/src/main/java/com/wagnergerald/salapay/server/server/security)
 * Transaction handling will be handled via a JMS (Java messaging service) in the activeMQ implementation
   * Advantages:
     * Submitted transactions will be processed in order, but only when one worker is used
     * Load balancing and performance gains are possible easily, by simply increasing the worker count, synchronization
       would be needed though.
     * No message will be missed, as they are persisted into an separate [KahaDB](https://activemq.apache.org/kahadb):
   * Disadvantages
     * Exact order can't be guaranteed for more than one worker, unless 
       [JMS message groups](https://activemq.apache.org/message-groups.html) are used e.g. on a user oder currency basis.
 * Data Model
   * For persistence JPA is used
   * To increase extensability an own wallet table is used to separate the crypto coin specifics from the user table
     * Coin-specific parameters can modeled e.g. with JPA inheritance mechanisms
   * Associate transaction to wallets an not to users, therefore wallet-ownership can be changed easily.
 
Frontend (planned, not implemented yet)
 * Frontend ReactJS + Material UI
 * Endpoint protection via spring boot security

## Architecture

![Kiku](./docs/SalaPay-Doc-Architecture.PNG)

## Documentation

## Time Estimations

All initial durations for the tasks were estimates based on my experience without tolerance.

| Task                                              | Estim. [h] | Time needed [h]|
| ------------------------------------------------- |:----------:| --------------:|
| #1 Backend API is defined with swagger            |          2 |     -          |
| #2 Working project setup                          |          2 |     -          |
| #3 Data model is defined and schema is available· |          1 |     -          |
| #4 Swagger REST API endpoints are available·      |          1 |     -          |
| (not in initial estimation) deployment and setup  |          3 |     -          |
| Overall                                           |     **10** |    **30,5**    |               

## Notes on the time estimation:

My estimation was quite far away from the time actual spent on the projet so far, some reasons are:
 * Using swagger as a tool API design tool complicated the project to in some ways, as I had to separate and map 
   everything between API and backend model.
 * I used the possibility to read into quite a lot of topics into more detail like
   * E.g. Swagger OPENAPI definition, tools and utils
   * JPA/Hibernate, Validation, queries, JPQL 
   * Reading into Spring/JUnit 5 capabilities
 * The project setup itself took longer than expected
 * I didn't plan to provide an live instance

# Open TODO's & FIXME's:
## Backend

 * Queue optimizations
   * Add synchronization and more workers
   * Ensure order for exact order for users or currencies with message groups
 * Implement user rights management, now API and "web" users are stored differently
 * Improve test-suite with Spring framework capabilities 

## Frontend
 * Project setup
 * Bundling/minifying
 * JS Optimizing
 * Production CDN
 * Linting
 * Checkstyle
 * Improve fronted user-feedback
 * See further TODO's in Code

# Research & further links
 * Spring fox swagger
 * ReactJS minimal Startup Setup 

 * Swagger
   * https://github.com/swagger-api/swagger-ui
   * https://apitools.dev/swagger-cli/
   * https://swagger.io/docs/open-source-tools/swagger-codegen/
   * https://openapi-generator.tech

  * Generate the code
    * https://openapi-generator.tech/docs/usage
    * Install the tool: npm install @openapitools/openapi-generator-cli -g
    * Generate the code with:
        ```bash
        npx openapi-generator generate -g spring -i swagger_vWork.yaml --model-package com.wagnergerald.salapay.api.model -c config.json -p com.wagnergerald.salapay -o ~/tmp/swagger-test/
        ```
