package com.wagnergerald.salapay.api;

import com.wagnergerald.salapay.api.model.CreateUser;
import com.wagnergerald.salapay.api.model.User;
import com.wagnergerald.salapay.api.model.Wallet;
import com.wagnergerald.salapay.api.util.ArrayUtils;
import com.wagnergerald.salapay.api.util.ErrorHandler;
import com.wagnergerald.salapay.server.server.controller.IUserController;
import com.wagnergerald.salapay.server.server.controller.IWalletController;
import com.wagnergerald.salapay.server.server.model.SalaPayUser;
import com.wagnergerald.salapay.server.server.model.SalaPayUserStatus;
import com.wagnergerald.salapay.server.server.model.SalaPayWallet;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.ExceptionHandler;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.context.request.NativeWebRequest;

import javax.validation.*;
import java.util.Collections;
import java.util.List;
import java.util.Optional;
import java.util.Set;
import java.util.stream.Collectors;

@Controller
@RequestMapping("${openapi.salaPay.base-path:/api/1.0.0}")
public class UserApiController implements UserApi {

    private final NativeWebRequest request;
    private final IUserController userController;
    private final IWalletController walletController;

    @org.springframework.beans.factory.annotation.Autowired
    public UserApiController(NativeWebRequest request, IUserController userController, IWalletController walletController) {
        this.request = request;
        this.userController = userController;
        this.walletController = walletController;
    }

    @Override
    public Optional<NativeWebRequest> getRequest() {
        return Optional.ofNullable(request);
    }

    @Override
    public ResponseEntity<User> getUser(Long userId) {
        return getUserResponseEntity(userId);
    }

    private ResponseEntity<User> getUserResponseEntity(Long userId) {
        Optional<SalaPayUser> user = userController.getUserById(userId);
        return user
                .map(u -> new ResponseEntity<>(createCreateResponse(u), HttpStatus.OK))
                .orElseGet(() -> new ResponseEntity<>(HttpStatus.NOT_FOUND));
    }

    private User createCreateResponse(SalaPayUser user) {
        List<SalaPayWallet> walletsForUser = walletController.getWalletsForUser(user).orElse(Collections.EMPTY_LIST);
        List<Wallet> collect = walletsForUser.stream()
                .map(this::convertWallet)
                .collect(Collectors.toList());

        User result = new User();
        result.setId(user.getId());
        result.setDescription(user.getDescription());
        result.setEmail(user.getEmail());
        result.setUsername(user.getUsername());
        result.setFirstName(user.getFirstName());
        result.setLastName(user.getLastName());
        result.setPhone(user.getPhone());
        result.setTransactionLimit(user.getTransactionLimit());
        result.setUserStatus(ArrayUtils.convertArray(user.getUserStatus()));
        result.setWallets(collect);

        return result;
    }

    private Wallet convertWallet(SalaPayWallet w) {
        Wallet wallet = new Wallet();
        wallet.setBalance(w.getBalance());
        wallet.setCurrency(ArrayUtils.convertArray(w.getCurrency()));
        wallet.setId(w.getId());
        return wallet;
    }

    @Override
    public ResponseEntity<User> createUser(@Valid CreateUser createUser) {
        SalaPayUser createRequest = createUserObject(createUser);
        String violations = validateUserObject(createRequest);
        if ((violations != null) && !violations.isEmpty()) {
            throw new ValidationException(violations);
        }
        SalaPayUser userEntity = userController.createUser(createRequest);
        return new ResponseEntity<>(createCreateResponse(userEntity), HttpStatus.OK);
    }

    private String validateUserObject(SalaPayUser createUser) {
        ValidatorFactory factory = Validation.buildDefaultValidatorFactory();
        Validator validator = factory.getValidator();
        Set<ConstraintViolation<SalaPayUser>> violations = validator.validate(createUser);
        return violations.stream().map(ConstraintViolation::getMessage).collect(Collectors.joining(","));

    }

    private SalaPayUser createUserObject(@Valid CreateUser createUser) {
        SalaPayUser user = SalaPayUser.builder().
                description(createUser.getDescription()).
                email(createUser.getEmail()).
                username(createUser.getUsername()).
                firstName(createUser.getFirstName()).
                lastName(createUser.getLastName()).
                phone(createUser.getPhone()).
                transactionLimit(createUser.getTransactionLimit()).
                userStatus(SalaPayUserStatus.ACTIVE).build();
        //TODO In production use a secure String implementation to avoid saving passwords plain in memory
        //https://docs.oracle.com/html/E28160_01/org/identityconnectors/common/security/GuardedString.html
        user.setPlainPassword(createUser.getPassword());
        return user;
    }

    @ExceptionHandler(ValidationException.class)
    public ResponseEntity handleException(ValidationException e) {
        return ErrorHandler.handleException(e.getMessage(), e);
    }

    @ExceptionHandler(Exception.class)
    public ResponseEntity handleAllOtherExceptions(Exception e) {
        return ErrorHandler.handleException("Uncategorized error, please contact the administrator!", e);
    }

}
