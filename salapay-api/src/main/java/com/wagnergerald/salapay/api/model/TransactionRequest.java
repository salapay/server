package com.wagnergerald.salapay.api.model;

import com.fasterxml.jackson.annotation.JsonProperty;
import io.swagger.annotations.ApiModelProperty;

import javax.validation.Valid;
import java.util.Objects;

/**
 * TransactionRequest
 */

public class TransactionRequest   {
  @JsonProperty("currency")
  private Currency currency;

  @JsonProperty("amount")
  private Double amount;

  @JsonProperty("sourceUserId")
  private Long sourceUserId;

  @JsonProperty("targetUserId")
  private Long targetUserId;

  public TransactionRequest currency(Currency currency) {
    this.currency = currency;
    return this;
  }

  /**
   * Get currency
   * @return currency
  */
  @ApiModelProperty(value = "")

  @Valid

  public Currency getCurrency() {
    return currency;
  }

  public void setCurrency(Currency currency) {
    this.currency = currency;
  }

  public TransactionRequest amount(Double amount) {
    this.amount = amount;
    return this;
  }

  /**
   * Get amount
   * @return amount
  */
  @ApiModelProperty(value = "")


  public Double getAmount() {
    return amount;
  }

  public void setAmount(Double amount) {
    this.amount = amount;
  }

  public TransactionRequest sourceUserId(Long sourceUserId) {
    this.sourceUserId = sourceUserId;
    return this;
  }

  /**
   * Get sourceUserId
   * @return sourceUserId
  */
  @ApiModelProperty(value = "")


  public Long getSourceUserId() {
    return sourceUserId;
  }

  public void setSourceUserId(Long sourceUserId) {
    this.sourceUserId = sourceUserId;
  }

  public TransactionRequest targetUserId(Long targetUserId) {
    this.targetUserId = targetUserId;
    return this;
  }

  /**
   * Get targetUserId
   * @return targetUserId
  */
  @ApiModelProperty(value = "")


  public Long getTargetUserId() {
    return targetUserId;
  }

  public void setTargetUserId(Long targetUserId) {
    this.targetUserId = targetUserId;
  }


  @Override
  public boolean equals(java.lang.Object o) {
    if (this == o) {
      return true;
    }
    if (o == null || getClass() != o.getClass()) {
      return false;
    }
    TransactionRequest transactionRequest = (TransactionRequest) o;
    return Objects.equals(this.currency, transactionRequest.currency) &&
        Objects.equals(this.amount, transactionRequest.amount) &&
        Objects.equals(this.sourceUserId, transactionRequest.sourceUserId) &&
        Objects.equals(this.targetUserId, transactionRequest.targetUserId);
  }

  @Override
  public int hashCode() {
    return Objects.hash(currency, amount, sourceUserId, targetUserId);
  }

  @Override
  public String toString() {
    StringBuilder sb = new StringBuilder();
    sb.append("class TransactionRequest {\n");
    
    sb.append("    currency: ").append(toIndentedString(currency)).append("\n");
    sb.append("    amount: ").append(toIndentedString(amount)).append("\n");
    sb.append("    sourceUserId: ").append(toIndentedString(sourceUserId)).append("\n");
    sb.append("    targetUserId: ").append(toIndentedString(targetUserId)).append("\n");
    sb.append("}");
    return sb.toString();
  }

  /**
   * Convert the given object to string with each line indented by 4 spaces
   * (except the first line).
   */
  private String toIndentedString(java.lang.Object o) {
    if (o == null) {
      return "null";
    }
    return o.toString().replace("\n", "\n    ");
  }
}

