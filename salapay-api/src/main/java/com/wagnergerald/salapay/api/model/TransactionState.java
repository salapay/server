package com.wagnergerald.salapay.api.model;

import java.util.Objects;
import com.fasterxml.jackson.annotation.JsonValue;
import org.openapitools.jackson.nullable.JsonNullable;
import javax.validation.Valid;
import javax.validation.constraints.*;

import com.fasterxml.jackson.annotation.JsonCreator;

/**
 * Gets or Sets TransactionState
 */
public enum TransactionState {
  
  REJECTED("REJECTED"),
  
  ACCEPTED_PENDING("ACCEPTED_PENDING"),
  
  ACCEPTED_PROCESSED("ACCEPTED_PROCESSED");

  private String value;

  TransactionState(String value) {
    this.value = value;
  }

  @JsonValue
  public String getValue() {
    return value;
  }

  @Override
  public String toString() {
    return String.valueOf(value);
  }

  @JsonCreator
  public static TransactionState fromValue(String value) {
    for (TransactionState b : TransactionState.values()) {
      if (b.value.equals(value)) {
        return b;
      }
    }
    throw new IllegalArgumentException("Unexpected value '" + value + "'");
  }
}

