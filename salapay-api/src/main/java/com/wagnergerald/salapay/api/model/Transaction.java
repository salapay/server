package com.wagnergerald.salapay.api.model;

import java.util.Objects;
import com.fasterxml.jackson.annotation.JsonProperty;
import com.fasterxml.jackson.annotation.JsonCreator;
import com.wagnergerald.salapay.api.model.Currency;
import com.wagnergerald.salapay.api.model.TransactionState;
import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import java.time.OffsetDateTime;
import java.util.UUID;
import org.openapitools.jackson.nullable.JsonNullable;
import javax.validation.Valid;
import javax.validation.constraints.*;

/**
 * Transaction
 */

public class Transaction   {
  @JsonProperty("identifier")
  private UUID identifier;

  @JsonProperty("amount")
  private Double amount;

  @JsonProperty("currency")
  private Currency currency;

  @JsonProperty("sourceUserId")
  private Long sourceUserId;

  @JsonProperty("targetUserId")
  private Long targetUserId;

  @JsonProperty("timestampCreated")
  private OffsetDateTime timestampCreated;

  @JsonProperty("timestampProcessed")
  private OffsetDateTime timestampProcessed;

  @JsonProperty("state")
  private TransactionState state;

  public Transaction identifier(UUID identifier) {
    this.identifier = identifier;
    return this;
  }

  /**
   * Get identifier
   * @return identifier
  */
  @ApiModelProperty(value = "")

  @Valid

  public UUID getIdentifier() {
    return identifier;
  }

  public void setIdentifier(UUID identifier) {
    this.identifier = identifier;
  }

  public Transaction amount(Double amount) {
    this.amount = amount;
    return this;
  }

  /**
   * Get amount
   * @return amount
  */
  @ApiModelProperty(value = "")


  public Double getAmount() {
    return amount;
  }

  public void setAmount(Double amount) {
    this.amount = amount;
  }

  public Transaction currency(Currency currency) {
    this.currency = currency;
    return this;
  }

  /**
   * Get currency
   * @return currency
  */
  @ApiModelProperty(value = "")

  @Valid

  public Currency getCurrency() {
    return currency;
  }

  public void setCurrency(Currency currency) {
    this.currency = currency;
  }

  public Transaction sourceUserId(Long sourceUserId) {
    this.sourceUserId = sourceUserId;
    return this;
  }

  /**
   * Get sourceUserId
   * @return sourceUserId
  */
  @ApiModelProperty(value = "")


  public Long getSourceUserId() {
    return sourceUserId;
  }

  public void setSourceUserId(Long sourceUserId) {
    this.sourceUserId = sourceUserId;
  }

  public Transaction targetUserId(Long targetUserId) {
    this.targetUserId = targetUserId;
    return this;
  }

  /**
   * Get targetUserId
   * @return targetUserId
  */
  @ApiModelProperty(value = "")


  public Long getTargetUserId() {
    return targetUserId;
  }

  public void setTargetUserId(Long targetUserId) {
    this.targetUserId = targetUserId;
  }

  public Transaction timestampCreated(OffsetDateTime timestampCreated) {
    this.timestampCreated = timestampCreated;
    return this;
  }

  /**
   * Get timestampCreated
   * @return timestampCreated
  */
  @ApiModelProperty(value = "")

  @Valid

  public OffsetDateTime getTimestampCreated() {
    return timestampCreated;
  }

  public void setTimestampCreated(OffsetDateTime timestampCreated) {
    this.timestampCreated = timestampCreated;
  }

  public Transaction timestampProcessed(OffsetDateTime timestampProcessed) {
    this.timestampProcessed = timestampProcessed;
    return this;
  }

  /**
   * Get timestampProcessed
   * @return timestampProcessed
  */
  @ApiModelProperty(value = "")

  @Valid

  public OffsetDateTime getTimestampProcessed() {
    return timestampProcessed;
  }

  public void setTimestampProcessed(OffsetDateTime timestampProcessed) {
    this.timestampProcessed = timestampProcessed;
  }

  public Transaction state(TransactionState state) {
    this.state = state;
    return this;
  }

  /**
   * Get state
   * @return state
  */
  @ApiModelProperty(value = "")

  @Valid

  public TransactionState getState() {
    return state;
  }

  public void setState(TransactionState state) {
    this.state = state;
  }


  @Override
  public boolean equals(java.lang.Object o) {
    if (this == o) {
      return true;
    }
    if (o == null || getClass() != o.getClass()) {
      return false;
    }
    Transaction transaction = (Transaction) o;
    return Objects.equals(this.identifier, transaction.identifier) &&
        Objects.equals(this.amount, transaction.amount) &&
        Objects.equals(this.currency, transaction.currency) &&
        Objects.equals(this.sourceUserId, transaction.sourceUserId) &&
        Objects.equals(this.targetUserId, transaction.targetUserId) &&
        Objects.equals(this.timestampCreated, transaction.timestampCreated) &&
        Objects.equals(this.timestampProcessed, transaction.timestampProcessed) &&
        Objects.equals(this.state, transaction.state);
  }

  @Override
  public int hashCode() {
    return Objects.hash(identifier, amount, currency, sourceUserId, targetUserId, timestampCreated, timestampProcessed, state);
  }

  @Override
  public String toString() {
    StringBuilder sb = new StringBuilder();
    sb.append("class Transaction {\n");
    
    sb.append("    identifier: ").append(toIndentedString(identifier)).append("\n");
    sb.append("    amount: ").append(toIndentedString(amount)).append("\n");
    sb.append("    currency: ").append(toIndentedString(currency)).append("\n");
    sb.append("    sourceUserId: ").append(toIndentedString(sourceUserId)).append("\n");
    sb.append("    targetUserId: ").append(toIndentedString(targetUserId)).append("\n");
    sb.append("    timestampCreated: ").append(toIndentedString(timestampCreated)).append("\n");
    sb.append("    timestampProcessed: ").append(toIndentedString(timestampProcessed)).append("\n");
    sb.append("    state: ").append(toIndentedString(state)).append("\n");
    sb.append("}");
    return sb.toString();
  }

  /**
   * Convert the given object to string with each line indented by 4 spaces
   * (except the first line).
   */
  private String toIndentedString(java.lang.Object o) {
    if (o == null) {
      return "null";
    }
    return o.toString().replace("\n", "\n    ");
  }
}

