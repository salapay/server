package com.wagnergerald.salapay.api.util;

import com.wagnergerald.salapay.api.Util;
import com.wagnergerald.salapay.api.model.Currency;
import com.wagnergerald.salapay.api.model.TransactionState;
import com.wagnergerald.salapay.api.model.UserStatus;
import com.wagnergerald.salapay.server.server.model.SalaPayCurrency;
import com.wagnergerald.salapay.server.server.model.SalaPayTransactionState;
import com.wagnergerald.salapay.server.server.model.SalaPayUserStatus;

import java.util.EnumSet;

/**
 * Class for converting API model arrays to backend representation
 * * TODO research a better mechanism to convert arrays
 */
public class ArrayUtils {

    public static Currency convertArray(SalaPayCurrency currency) {
        if (currency == null)
            return null;
        return EnumSet.allOf(Currency.class).stream()
                .filter(c -> c.getValue().equals(currency.toString()))
                .collect(Util.toSingleton());
    }

    public static SalaPayCurrency convertArray(Currency currency) {
        if (currency == null)
            return null;
        return EnumSet.allOf(SalaPayCurrency.class).stream()
                .filter(c -> c.getValue().equals(currency.toString()))
                .collect(Util.toSingleton());
    }

    public static TransactionState convertArray(SalaPayTransactionState state) {
        if (state == null)
            return null;
        return EnumSet.allOf(TransactionState.class).stream()
                .filter(c -> c.getValue().equals(state.toString()))
                .collect(Util.toSingleton());
    }

    public static SalaPayTransactionState convertArray(TransactionState state) {
        if (state == null)
            return null;
        return EnumSet.allOf(SalaPayTransactionState.class).stream()
                .filter(c -> c.getValue().equals(state.toString()))
                .collect(Util.toSingleton());
    }

    public static SalaPayUserStatus convertArray(UserStatus state) {
        if (state == null)
            return null;
        return EnumSet.allOf(SalaPayUserStatus.class).stream()
                .filter(c -> c.getValue().equals(state.toString()))
                .collect(Util.toSingleton());
    }

    public static UserStatus convertArray(SalaPayUserStatus state) {
        if (state == null)
            return null;
        return EnumSet.allOf(UserStatus.class).stream()
                .filter(c -> c.getValue().equals(state.toString()))
                .collect(Util.toSingleton());
    }
}
