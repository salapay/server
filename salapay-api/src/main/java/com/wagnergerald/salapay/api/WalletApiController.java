package com.wagnergerald.salapay.api;

import com.wagnergerald.salapay.api.model.CreateWalletRequest;
import com.wagnergerald.salapay.api.model.Wallet;
import com.wagnergerald.salapay.api.util.ArrayUtils;
import com.wagnergerald.salapay.api.util.ErrorHandler;
import com.wagnergerald.salapay.server.server.controller.IWalletController;
import com.wagnergerald.salapay.server.server.controller.UserController;
import com.wagnergerald.salapay.server.server.model.SalaPayUser;
import com.wagnergerald.salapay.server.server.model.SalaPayWallet;
import io.swagger.annotations.ApiParam;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.ExceptionHandler;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.context.request.NativeWebRequest;

import javax.validation.Valid;
import javax.validation.ValidationException;
import java.util.Optional;

@Controller
@RequestMapping("${openapi.salaPay.base-path:/api/1.0.0}")
public class WalletApiController implements WalletApi {

    private final NativeWebRequest request;

    private IWalletController walletController;
    private UserController userController;

    @org.springframework.beans.factory.annotation.Autowired
    public WalletApiController(NativeWebRequest request, IWalletController walletController, UserController userController) {
        this.request = request;
        this.walletController = walletController;
        this.userController = userController;
    }

    @Override
    public Optional<NativeWebRequest> getRequest() {
        return Optional.ofNullable(request);
    }

    @Override
    public ResponseEntity<Wallet> createUserWallet(@ApiParam(value = "Created user object", required = true) @Valid @RequestBody CreateWalletRequest createWalletRequest) {
        return userController.getUserById(createWalletRequest.getUserId())
                .map(user -> new ResponseEntity<>(createCreateWalletResponse(user, createWalletRequest), HttpStatus.OK))
                .orElseGet(() -> new ResponseEntity<>(null, HttpStatus.INTERNAL_SERVER_ERROR));
    }

    private Wallet createCreateWalletResponse(SalaPayUser user, CreateWalletRequest createWalletRequest) {
        SalaPayWallet wallet = walletController.createWalletForUser(user, ArrayUtils.convertArray(createWalletRequest.getCurrency()));
        return createCreateResponse(wallet);
    }

    @Override
    public ResponseEntity<Wallet> getWallet(Long walletId) {
        SalaPayWallet wallet = walletController.getWalletById(walletId);
        return Optional.of(wallet)
                .map(value -> new ResponseEntity<>(createCreateResponse(value), HttpStatus.OK))
                .orElseGet(() -> new ResponseEntity<>(null, HttpStatus.NOT_FOUND));
    }

    private Wallet createCreateResponse(SalaPayWallet wallet) {
        Wallet apiWallet = new Wallet();
        apiWallet.setId(wallet.getId());
        apiWallet.setBalance(wallet.getBalance());
        apiWallet.setCurrency(ArrayUtils.convertArray(wallet.getCurrency()));
        return apiWallet;
    }

    @ExceptionHandler(ValidationException.class)
    public ResponseEntity handleException(ValidationException e) {
        return ErrorHandler.handleException(e.getMessage(), e);
    }

    @ExceptionHandler(Exception.class)
    public ResponseEntity handleAllOtherExceptions(Exception e) {
        return ErrorHandler.handleException("Uncategorized error, please contact the administrator!", e);
    }

}
