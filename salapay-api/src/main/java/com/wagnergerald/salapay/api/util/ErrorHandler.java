package com.wagnergerald.salapay.api.util;

import com.wagnergerald.salapay.api.model.ErrorResponse;
import lombok.extern.slf4j.Slf4j;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;

@Slf4j
public class ErrorHandler {

    public static ResponseEntity handleException(String s, Exception e) {
        log.error("Could not handle request: ", e);
        ErrorResponse errorResponse = new ErrorResponse();
        errorResponse.setDescription(s);
        return ResponseEntity
                .status(HttpStatus.INTERNAL_SERVER_ERROR)
                .body(errorResponse);
    }
}
