package com.wagnergerald.salapay.api;

import com.wagnergerald.salapay.api.model.Transaction;
import com.wagnergerald.salapay.api.model.TransactionRequest;
import com.wagnergerald.salapay.api.util.ArrayUtils;
import com.wagnergerald.salapay.api.util.ErrorHandler;
import com.wagnergerald.salapay.server.server.controller.ITransactionController;
import com.wagnergerald.salapay.server.server.model.SalaPayTransaction;
import io.swagger.annotations.ApiParam;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.ExceptionHandler;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.context.request.NativeWebRequest;

import javax.validation.Valid;
import javax.validation.ValidationException;
import java.util.List;
import java.util.Optional;
import java.util.UUID;
import java.util.stream.Collectors;

@Controller
@RequestMapping("${openapi.salaPay.base-path:/api/1.0.0}")
public class TransactionApiController implements TransactionApi {

    private final NativeWebRequest request;

    private final ITransactionController transactionController;

    @org.springframework.beans.factory.annotation.Autowired
    public TransactionApiController(NativeWebRequest request, ITransactionController transactionController) {
        this.request = request;
        this.transactionController = transactionController;
    }

    @Override
    public Optional<NativeWebRequest> getRequest() {
        return Optional.ofNullable(request);
    }

    @Override
    public ResponseEntity<Transaction> submitTransaction(@Valid TransactionRequest transactionRequest) {
        SalaPayTransaction transaction = handleRequest(transactionRequest);
        Transaction response = buildResponse(transaction);
        return new ResponseEntity<>(response, HttpStatus.OK);
    }

    private SalaPayTransaction handleRequest(@Valid TransactionRequest request) {
        return transactionController.handleTransactionRequest(request.getSourceUserId(), request.getTargetUserId(), ArrayUtils.convertArray(request.getCurrency()), request.getAmount());
    }

    private List<Transaction> buildResponse(List<SalaPayTransaction> transactions) {
        return transactions.stream()
                .map(this::buildResponse)
                .collect(Collectors.toList());
    }

    private Transaction buildResponse(SalaPayTransaction transaction) {
        Transaction result = new Transaction();
        result.setAmount(transaction.getAmount());
        result.setCurrency(ArrayUtils.convertArray(transaction.getCurrency()));
        result.setTimestampCreated(transaction.getTimestampCreated());
        result.setTimestampProcessed(transaction.getTimestampProcessed());
        result.setState(ArrayUtils.convertArray(transaction.getState()));
        result.setIdentifier(transaction.getUuid());
        result.setSourceUserId(transactionController.getUserIdForWallet(transaction.getSrcWallet()));
        result.setTargetUserId(transactionController.getUserIdForWallet(transaction.getDestWallet()));
        return result;
    }

    @Override
    public ResponseEntity<Transaction> getTransactionGetStatus(@ApiParam(value = "transactionId", required = true) @PathVariable("transactionId") UUID transactionId) {
        Optional<SalaPayTransaction> transaction = transactionController.getTransactionById(transactionId);

        return transaction.map(u -> new ResponseEntity<Transaction>(buildResponse(u), HttpStatus.OK))
                .orElseGet(() -> new ResponseEntity<>(HttpStatus.NOT_FOUND));
    }


    @Override
    public ResponseEntity<List<Transaction>> getUserTransactions(@ApiParam(value = "userId", required = true) @PathVariable("userId") Long userId) {
        Optional<List<SalaPayTransaction>> list = transactionController.transactionsForUser(userId);
        return list
                .map(u -> new ResponseEntity<>(buildResponse(u), HttpStatus.OK))
                .orElseGet(() -> new ResponseEntity<>(HttpStatus.NOT_FOUND));
    }


    @ExceptionHandler(ValidationException.class)
    public ResponseEntity handleException(ValidationException e) {
        return ErrorHandler.handleException(e.getMessage(), e);
    }

    @ExceptionHandler(Exception.class)
    public ResponseEntity handleAllOtherExceptions(Exception e) {
        return ErrorHandler.handleException("Uncategorized error, please contact the administrator!", e);
    }

}
