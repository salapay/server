package com.wagnergerald.salapay.server;

import com.wagnergerald.salapay.server.server.TestStartupListener;
import com.wagnergerald.salapay.server.server.controller.TransactionController;
import com.wagnergerald.salapay.server.server.model.*;
import com.wagnergerald.salapay.server.server.repo.ITransactionRepository;
import com.wagnergerald.salapay.server.server.repo.IUserRepository;
import com.wagnergerald.salapay.server.server.repo.IWalletRepository;
import lombok.extern.slf4j.Slf4j;
import org.junit.jupiter.api.BeforeAll;
import org.junit.jupiter.api.DisplayName;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.TestInstance;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.orm.jpa.DataJpaTest;

import static org.junit.Assert.assertEquals;
import static org.junit.jupiter.api.Assertions.assertNotNull;

@DataJpaTest
@DisplayName("SalaPay JPA repository")
@TestInstance(TestInstance.Lifecycle.PER_CLASS)
@Slf4j
class RepositoryTest {

    public static final String TEST_1 = "test1";
    public static final String TEST_2 = "test2";
    public static final SalaPayCurrency TEST_CURRENCY = SalaPayCurrency.BITCOIN;
    private IWalletRepository walletRepo;
    private IUserRepository userRepo;
    private ITransactionRepository transactionRepo;

    @Autowired
    RepositoryTest(IWalletRepository walletRepo, IUserRepository userRepo, ITransactionRepository transactionRepo) {
        this.walletRepo = walletRepo;
        this.userRepo = userRepo;
        this.transactionRepo = transactionRepo;
    }

    @BeforeAll
    void init() {
        SalaPayUser test1 = createUser(TEST_1);
        SalaPayUser test2 = createUser(TEST_2);

        createTransaction(test1, test2);
    }

    private void createTransaction(SalaPayUser test1, SalaPayUser test2) {
        SalaPayWallet srcWallet = TransactionController.getWallet(walletRepo, test1.getId(), TEST_CURRENCY, "not found");
        SalaPayWallet destWallet = TransactionController.getWallet(walletRepo, test1.getId(), TEST_CURRENCY, "not found");

        SalaPayTransaction transaction = SalaPayTransaction.builder().
                amount(1000.0).
                srcWallet(srcWallet).
                destWallet(destWallet).
                currency(TEST_CURRENCY).
                state(SalaPayTransactionState.ACCEPTED_PROCESSED).build();
        transactionRepo.save(transaction);
    }

    private SalaPayUser createUser(String test) {
        SalaPayUser user = new SalaPayUser();
        user.setUsername(test);
        user.setPlainPassword(test);
        user.setTransactionLimit(1000.0);
        user.setUserStatus(SalaPayUserStatus.ACTIVE);
        SalaPayWallet salaPayWallet = new SalaPayWallet();
        salaPayWallet.setBalance(TestStartupListener.DEFAULT_BALANCE);
        salaPayWallet.setCurrency(SalaPayCurrency.BITCOIN);
        salaPayWallet.setUser(user);
        salaPayWallet.setTransactionLimit(1000.0);
        user = userRepo.save(user);
        walletRepo.save(salaPayWallet);
        return user;
    }

    @Test
    @DisplayName("Wallet repository")
    void testWalletRepo() {
        assertEquals(2, walletRepo.count());
        assertEquals(2, userRepo.count());

        SalaPayWallet wallet = walletRepo.findWalletByUserAndCurrency(userRepo.findAll().iterator().next().getId(), SalaPayCurrency.BITCOIN).orElseThrow(() -> new RuntimeException("not found"));
        assertNotNull(wallet);
    }

    @Test
    @DisplayName("User repository")
    void testUserRepo() {
        SalaPayTransaction transaction = transactionRepo.findAll().iterator().next();
        Long id = transaction.getSrcWallet().getId();

        log.info("wallet-id={}", id);
        SalaPayUser user = userRepo.findUserForWalletId(id).orElseThrow(RuntimeException::new);
        assertNotNull(user);
        log.info("user-id={}", user.getId());

    }
}
