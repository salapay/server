package com.wagnergerald.salapay.server;

import org.springframework.boot.autoconfigure.SpringBootApplication;

// just needed for the reason of being there, for details see:
// https://www.baeldung.com/spring-boot-unable-to-find-springbootconfiguration-with-datajpatest
@SpringBootApplication
public class TestApp {
}
