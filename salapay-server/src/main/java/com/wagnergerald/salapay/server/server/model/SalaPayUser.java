package com.wagnergerald.salapay.server.server.model;

import lombok.*;
import org.hibernate.validator.constraints.Length;
import org.springframework.security.crypto.bcrypt.BCryptPasswordEncoder;
import org.springframework.security.crypto.password.PasswordEncoder;

import javax.persistence.*;
import javax.validation.constraints.Email;
import javax.validation.constraints.NotEmpty;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.PositiveOrZero;
import java.util.Set;

@Entity
@Setter
@Getter
@NoArgsConstructor
@AllArgsConstructor
@Builder
public class SalaPayUser {

    public static final PasswordEncoder PASSWORD_ENCODER = new BCryptPasswordEncoder();

    @Id
    @GeneratedValue
    private Long id;

    @Length(max = 10000)
    private String description;

    //Bitcoin: 26-35 alphanumeric characters
    @Length(min = 3, max = 512)
    @Column(unique = true)
    private String username;

    @Length(max = 1000)
    @Column(unique = true)
    @Email(message = "Email should be valid")
    private String email;

    private String firstName;
    private String lastName;
    private String phone;

    @NotEmpty
    private String password;

    @PositiveOrZero
    private Double transactionLimit;

    @NotNull
    @Enumerated(EnumType.STRING)
    private SalaPayUserStatus userStatus;

    @ManyToMany(fetch = FetchType.EAGER)
    private Set<SalaPayRole> roles;

    @Transient
    public void setPlainPassword(String password) {
        this.password = PASSWORD_ENCODER.encode(password);
    }

}
