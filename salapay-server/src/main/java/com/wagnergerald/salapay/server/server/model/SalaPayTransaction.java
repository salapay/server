package com.wagnergerald.salapay.server.server.model;

import lombok.*;
import org.hibernate.annotations.CreationTimestamp;
import org.hibernate.annotations.GenericGenerator;

import javax.annotation.CheckForNull;
import javax.persistence.*;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.PositiveOrZero;
import java.time.OffsetDateTime;
import java.util.UUID;

@Entity
@Setter
@Getter
@NoArgsConstructor
@AllArgsConstructor
@Builder
public class SalaPayTransaction {

    @Id
    @GeneratedValue
    private Long id;

    @NotNull
    @Column(unique = true)
    @GeneratedValue(generator = "UUID")
    @GenericGenerator(
            name = "UUID",
            strategy = "org.hibernate.id.UUIDGenerator"
    )
    private UUID uuid;

    @NotNull
    @PositiveOrZero
    private Double amount;

    @NotNull
    private SalaPayCurrency currency;

    @NotNull
    @ManyToOne
    private SalaPayWallet srcWallet;

    @NotNull
    @ManyToOne
    private SalaPayWallet destWallet;

    @CheckForNull
    @CreationTimestamp
    private OffsetDateTime timestampCreated;

    private OffsetDateTime timestampProcessed;

    @NotNull
    @Enumerated(EnumType.STRING)
    private SalaPayTransactionState state;

    @PrePersist
    public void prePersist() {
        if (uuid == null) {
            uuid = UUID.randomUUID();
        }
    }
}
