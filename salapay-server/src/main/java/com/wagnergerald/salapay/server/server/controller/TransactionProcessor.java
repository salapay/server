package com.wagnergerald.salapay.server.server.controller;

import com.wagnergerald.salapay.server.server.SalaPayConsts;
import com.wagnergerald.salapay.server.server.model.SalaPayTransaction;
import com.wagnergerald.salapay.server.server.model.SalaPayTransactionState;
import com.wagnergerald.salapay.server.server.model.SalaPayWallet;
import com.wagnergerald.salapay.server.server.repo.ITransactionRepository;
import com.wagnergerald.salapay.server.server.repo.IWalletRepository;
import lombok.extern.slf4j.Slf4j;
import org.springframework.jms.annotation.JmsListener;
import org.springframework.stereotype.Component;

import javax.transaction.Transactional;
import java.time.OffsetDateTime;
import java.util.Arrays;

@Slf4j
@Component
public class TransactionProcessor implements ITransactionProcessor {

    private ITransactionRepository transactionRepo;
    private IWalletRepository walletRepo;

    public TransactionProcessor(ITransactionRepository transactionRepo, IWalletRepository walletRepository) {
        this.transactionRepo = transactionRepo;
        this.walletRepo = walletRepository;
    }

    @Override
    @JmsListener(destination = SalaPayConsts.TRANSACTION_QUEUE, concurrency = "1")
    public void processAcceptedTransactionFromQueue(String message) throws RuntimeException {
        log.info("Received transaction with id {}", message);
        SalaPayTransaction trans = transactionRepo.findById(Long.valueOf(message)).orElseThrow(() -> new RuntimeException("could not find User Transaction"));
        handleAcceptedTransaction(trans);
        log.info("Successfully processed transaction with id {} ", message);
    }

    protected void handleAcceptedTransaction(SalaPayTransaction transaction) {
        log.debug("Start handling transaction ");
        SalaPayWallet src = transaction.getSrcWallet();
        SalaPayWallet dest = transaction.getDestWallet();

        //Note, currently no synchronisation is needed, as the queue events are processed serially.
        //For implementations with parallel transaction processors a locking mechanism would be needed.
        Double amount = Math.abs(transaction.getAmount());
        double newSrcBalance = src.getBalance() - amount;
        double newDestBalance = dest.getBalance() + amount;

        if (amount > src.getTransactionLimit()
                || amount > src.getUser().getTransactionLimit()
                || newSrcBalance < 0
                || newDestBalance > Double.parseDouble(SalaPayWallet.WALLET_MAX_VALUE)) {
            transaction.setState(SalaPayTransactionState.REJECTED);
        } else {
            src.setBalance(newSrcBalance);
            dest.setBalance(newDestBalance);
            transaction.setState(SalaPayTransactionState.ACCEPTED_PROCESSED);
        }
        persistTransaction(transaction, src, dest);

        log.info("Sent {}[{}] from srcWallet={} to destWallet={}, transactionId={}", transaction.getAmount(), transaction.getCurrency(), src.getId(), dest.getId(), transaction.getUuid());
    }

    @Transactional(Transactional.TxType.REQUIRES_NEW)
    public void persistTransaction(SalaPayTransaction transaction, SalaPayWallet src, SalaPayWallet dest) {
        walletRepo.saveAll(Arrays.asList(src, dest));
        transaction.setTimestampProcessed(OffsetDateTime.now());
        transactionRepo.save(transaction);
    }

}
