package com.wagnergerald.salapay.server.server.model;

public enum SalaPayCurrency {

    ETHEREUM("Ethereum"),

    BITCOIN("Bitcoin"),

    // add more currencies
    BITCOINCASH("BitcoinCash");

    private String value;

    SalaPayCurrency(String value) {
        this.value = value;
    }

    public String getValue() {
        return value;
    }

    @Override
    public String toString() {
        return String.valueOf(value);
    }

}

