package com.wagnergerald.salapay.server.server;

import com.wagnergerald.salapay.server.server.model.*;
import com.wagnergerald.salapay.server.server.repo.IRoleRepository;
import com.wagnergerald.salapay.server.server.repo.IUserRepository;
import com.wagnergerald.salapay.server.server.repo.IWalletRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import javax.annotation.PostConstruct;
import javax.transaction.Transactional;
import java.util.Arrays;
import java.util.EnumSet;
import java.util.List;
import java.util.Set;
import java.util.stream.Collectors;

@Component
public class TestStartupListener {

    public static final double DEFAULT_BALANCE = 1000.;
    private IUserRepository userRepo;
    private IRoleRepository roleRepo;
    private IWalletRepository walletRepo;

    @Autowired
    public TestStartupListener(IUserRepository userRepo, IRoleRepository roleRepo, IWalletRepository walletRepo) {
        this.userRepo = userRepo;
        this.roleRepo = roleRepo;
        this.walletRepo = walletRepo;
    }

    @PostConstruct
    public void init() {
        createRoles();

        createUser("gerald", "gerald", true, SalaPayConsts.ROLE_ADMIN, SalaPayConsts.ROLE_API, SalaPayConsts.ROLE_DOCS);
        createUser("test", "test", true);
        createUser("noWallets", "noWallets", false);
    }

    @Transactional(value = Transactional.TxType.REQUIRES_NEW)
    public void createRoles() {
        SalaPayConsts.ROLE_ALL.forEach(r -> {
            roleRepo.save(SalaPayRole.builder().name(r).build());
        });
    }

    private void createUser(String username, String password, boolean createWallets, String... roles) {
        SalaPayUser user = SalaPayUser.builder().username(username).build();
        user.setPlainPassword(password);
        user.setUserStatus(SalaPayUserStatus.ACTIVE);
        user.setTransactionLimit(DEFAULT_BALANCE);

        Set<SalaPayRole> rolesData = Arrays.stream(roles.clone())
                .map(r -> roleRepo.findByName(r))
                .collect(Collectors.toSet());
        user.setRoles(rolesData);
        userRepo.save(user);
        if (createWallets) {
            List<SalaPayWallet> wallets = EnumSet.allOf(SalaPayCurrency.class).stream()
                    .map(c -> SalaPayWallet.builder()
                            .user(user)
                            .currency(c)
                            .transactionLimit(DEFAULT_BALANCE)
                            .balance(DEFAULT_BALANCE).build())
                    .collect(Collectors.toList());
            walletRepo.saveAll(wallets);
        }
    }
}
