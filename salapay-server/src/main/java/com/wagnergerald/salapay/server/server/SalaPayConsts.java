package com.wagnergerald.salapay.server.server;

import java.util.Arrays;
import java.util.List;

public interface SalaPayConsts {

    String ROLE_ADMIN = "ADMIN";
    String ROLE_API = "API";
    String ROLE_DOCS = "DOCS";
    String ROLE_WEB = "WEB";
    List<String> ROLE_ALL = Arrays.asList(ROLE_ADMIN, ROLE_API, ROLE_DOCS, ROLE_WEB);

    String TRANSACTION_QUEUE = "transaction.q";
}
