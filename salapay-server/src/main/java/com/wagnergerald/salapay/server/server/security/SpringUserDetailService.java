package com.wagnergerald.salapay.server.server.security;

import com.wagnergerald.salapay.server.server.model.SalaPayUser;
import com.wagnergerald.salapay.server.server.repo.IUserRepository;
import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.security.core.userdetails.UserDetailsService;
import org.springframework.security.core.userdetails.UsernameNotFoundException;
import org.springframework.stereotype.Component;

import java.util.Collections;

@Component
public class SpringUserDetailService implements UserDetailsService {

    private final IUserRepository userRepository;

    public SpringUserDetailService(IUserRepository userRepository) {
        this.userRepository = userRepository;
    }

    @Override
    public UserDetails loadUserByUsername(String name) throws UsernameNotFoundException {
        SalaPayUser user = userRepository.findByUsername(name);
        return new org.springframework.security.core.userdetails.User(user.getUsername(), user.getPassword(), Collections.emptySet());
    }
}
