package com.wagnergerald.salapay.server.server.controller;

import com.wagnergerald.salapay.server.server.model.SalaPayCurrency;
import com.wagnergerald.salapay.server.server.model.SalaPayTransaction;
import com.wagnergerald.salapay.server.server.model.SalaPayTransactionState;
import com.wagnergerald.salapay.server.server.model.SalaPayWallet;
import com.wagnergerald.salapay.server.server.queue.SalaPayTransactionQueue;
import com.wagnergerald.salapay.server.server.repo.ITransactionRepository;
import com.wagnergerald.salapay.server.server.repo.IUserRepository;
import com.wagnergerald.salapay.server.server.repo.IWalletRepository;
import lombok.extern.slf4j.Slf4j;
import org.springframework.stereotype.Component;

import javax.transaction.Transactional;
import java.util.List;
import java.util.Optional;
import java.util.UUID;

@Component
@Slf4j
public class TransactionController implements ITransactionController {

    private SalaPayTransactionQueue transactionQueue;
    private ITransactionRepository transactionRepo;
    private IUserRepository userRepository;
    private IWalletRepository walletRepo;

    public TransactionController(SalaPayTransactionQueue transactionQueue, ITransactionRepository transactionRepo, IUserRepository userRepository, IWalletRepository walletRepo) {
        this.transactionQueue = transactionQueue;
        this.transactionRepo = transactionRepo;
        this.userRepository = userRepository;
        this.walletRepo = walletRepo;
    }

    public static SalaPayWallet getWallet(IWalletRepository walletRepo, Long userId, SalaPayCurrency currency, String message) {
        return walletRepo.findWalletByUserAndCurrency(userId, currency).orElseThrow(() -> new RuntimeException(message));
    }

    public Long getUserIdForWallet(SalaPayWallet wallet) {
        return userRepository.findUserForWalletId(wallet.getId()).orElseThrow(() -> new RuntimeException("Could not find user for wallet")).getId();
    }

    @Override
    public Optional<List<SalaPayTransaction>> transactionsForUser(Long userId) {
        return transactionRepo.findByUserId(userId);
    }

    @Override
    public SalaPayTransaction handleTransactionRequest(Long srcUserId, Long destUserId, SalaPayCurrency currency, Double amount) {
        SalaPayTransaction request = new SalaPayTransaction();
        try {
            request.setSrcWallet(getWallet(walletRepo, srcUserId, currency, "could not find src user or wallet"));
            request.setDestWallet(getWallet(walletRepo, destUserId, currency, "could not find dest user or wallet"));
            request.setAmount(amount);
            request.setCurrency(currency);
            request.setState(SalaPayTransactionState.ACCEPTED_PENDING);
        } catch (Exception e) {
            log.warn("could not process transaction, {}", e.getMessage());
            request.setState(SalaPayTransactionState.REJECTED);
        }

        return persistAndSendValidTransactionsToProcessors(request);
    }

    @Transactional
    protected SalaPayTransaction persistAndSendValidTransactionsToProcessors(SalaPayTransaction transaction) {
        log.info("Start persisting transaction");
        SalaPayTransaction persisted = transactionRepo.save(transaction);
        if (transaction.getState() == SalaPayTransactionState.ACCEPTED_PENDING) {
            log.debug("transaction accepted, sending it to queue");
            transactionQueue.submitTransaction(transaction.getId().toString());
            log.info("Sending {}[{}] from srcWallet={} to destWallet={}, transactionId={}",
                    transaction.getAmount(),
                    transaction.getCurrency(),
                    transaction.getSrcWallet().getId(),
                    transaction.getDestWallet().getId(),
                    transaction.getUuid());
        } else {
            log.info("Transaction was rejected, not sending it to queue id={}", transaction.getId());
        }

        return persisted;
    }

    public Optional<SalaPayTransaction> getTransactionById(UUID transactionId) {
        return transactionRepo.findByUuid(transactionId);
    }

}
