package com.wagnergerald.salapay.server.server.model;

public enum SalaPayUserStatus {

    ACTIVE("Active"),

    DEACTIVATED("Deactivated");

    private String value;

    SalaPayUserStatus(String value) {
        this.value = value;
    }

    public String getValue() {
        return value;
    }

    @Override
    public String toString() {
        return String.valueOf(value);
    }


}

