package com.wagnergerald.salapay.server.server.controller;

import com.wagnergerald.salapay.server.server.model.SalaPayCurrency;
import com.wagnergerald.salapay.server.server.model.SalaPayUser;
import com.wagnergerald.salapay.server.server.model.SalaPayWallet;

import java.util.List;
import java.util.Optional;

public interface IWalletController {

    SalaPayWallet getWalletById(Long walletId);

    SalaPayWallet createWalletForUser(SalaPayUser user, SalaPayCurrency currency);

    Optional<List<SalaPayWallet>> getWalletsForUser(SalaPayUser user);

}
