package com.wagnergerald.salapay.server.server.repo;

import com.wagnergerald.salapay.server.server.model.SalaPayRole;
import org.springframework.data.repository.CrudRepository;
import org.springframework.stereotype.Repository;

@Repository
public interface IRoleRepository extends CrudRepository<SalaPayRole, Long> {

    public SalaPayRole findByName(String name);

}

