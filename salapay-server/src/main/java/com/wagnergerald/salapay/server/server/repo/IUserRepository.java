package com.wagnergerald.salapay.server.server.repo;

import com.wagnergerald.salapay.server.server.model.SalaPayUser;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.CrudRepository;
import org.springframework.data.repository.query.Param;
import org.springframework.stereotype.Repository;

import java.util.Optional;

@Repository
public interface IUserRepository extends CrudRepository<SalaPayUser, Long> {

    public SalaPayUser findByUsername(String username);

    @Query("SELECT u FROM SalaPayUser u WHERE u.id = (SELECT w.user.id FROM SalaPayWallet w WHERE w.id = :walletId)")
    Optional<SalaPayUser> findUserForWalletId(@Param("walletId") Long walletId);

}
