package com.wagnergerald.salapay.server.server.controller;

import com.wagnergerald.salapay.server.server.model.SalaPayCurrency;
import com.wagnergerald.salapay.server.server.model.SalaPayUser;
import com.wagnergerald.salapay.server.server.model.SalaPayWallet;
import com.wagnergerald.salapay.server.server.repo.IWalletRepository;
import lombok.extern.slf4j.Slf4j;
import org.springframework.stereotype.Component;

import java.util.List;
import java.util.Optional;

@Component
@Slf4j
public class WalletController implements IWalletController {

    private IWalletRepository walletRepo;

    public WalletController(IWalletRepository walletRepo) {
        this.walletRepo = walletRepo;
    }

    public SalaPayWallet createWalletForUser(SalaPayUser user, SalaPayCurrency currency) {
        SalaPayWallet wallet = buildUser(user, currency);
        log.info("created wallet {} for user {}", wallet, user);
        return walletRepo.save(wallet);
    }

    public SalaPayWallet getWalletById(Long walletId) {
        return walletRepo.findById(walletId)
                .orElseThrow(() -> new RuntimeException("Could not find wallet with id " + walletId));
    }

    @Override
    public Optional<List<SalaPayWallet>> getWalletsForUser(SalaPayUser user) {
        return walletRepo.findByUser(user);
    }

    private SalaPayWallet buildUser(SalaPayUser user, SalaPayCurrency currency) {
        return SalaPayWallet.builder()
                .user(user)
                .currency(currency)
                .balance(1000.0)
                .transactionLimit(1000.0)
                .build();
    }

}
