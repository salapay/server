package com.wagnergerald.salapay.server.server.model;

import lombok.*;

import javax.persistence.*;
import javax.validation.constraints.DecimalMax;
import javax.validation.constraints.DecimalMin;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.PositiveOrZero;

@Entity
@Setter
@Getter
@NoArgsConstructor
@AllArgsConstructor
@Builder
@Table(uniqueConstraints = {@UniqueConstraint(columnNames = {"currency", "user_id"})})
public class SalaPayWallet {

    public static final String WALLET_MAX_VALUE = "1000000000.0";

    @Id
    @GeneratedValue
    private Long id;

    @NotNull
    private SalaPayCurrency currency;

    @NotNull
    @DecimalMin(".0")
    @DecimalMax(WALLET_MAX_VALUE)
    private Double balance;

    @NotNull
    @ManyToOne
    private SalaPayUser user;

    @NotNull
    @PositiveOrZero
    @DecimalMax(WALLET_MAX_VALUE)
    private Double transactionLimit;

}
