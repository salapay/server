package com.wagnergerald.salapay.server.server.controller;

import com.wagnergerald.salapay.server.server.model.SalaPayUser;
import com.wagnergerald.salapay.server.server.repo.IUserRepository;
import lombok.extern.slf4j.Slf4j;
import org.springframework.stereotype.Component;

import java.util.Optional;

@Component
@Slf4j
public class UserController implements IUserController {

    private IUserRepository userRepository;

    public UserController(IUserRepository userRepository) {
        this.userRepository = userRepository;
    }

    @Override
    public SalaPayUser createUser(SalaPayUser user) {
        SalaPayUser newUser = userRepository.save(user);
        log.info("created user {}", newUser.toString());
        return newUser;
    }

    @Override
    public Optional<SalaPayUser> getUserById(Long id) {
        return userRepository.findById(id);
    }

}
