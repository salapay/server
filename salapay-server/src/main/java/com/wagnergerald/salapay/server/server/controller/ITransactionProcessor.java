package com.wagnergerald.salapay.server.server.controller;

import com.wagnergerald.salapay.server.server.SalaPayConsts;
import org.springframework.jms.annotation.JmsListener;

public interface ITransactionProcessor {

    @JmsListener(destination = SalaPayConsts.TRANSACTION_QUEUE)
    void processAcceptedTransactionFromQueue(String message) throws RuntimeException;

}
