package com.wagnergerald.salapay.server.server.repo;

import com.wagnergerald.salapay.server.server.model.SalaPayCurrency;
import com.wagnergerald.salapay.server.server.model.SalaPayUser;
import com.wagnergerald.salapay.server.server.model.SalaPayWallet;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.CrudRepository;
import org.springframework.data.repository.query.Param;
import org.springframework.stereotype.Repository;

import java.util.List;
import java.util.Optional;

@Repository
public interface IWalletRepository extends CrudRepository<SalaPayWallet, Long> {

    Optional<SalaPayWallet> findByUserAndCurrency(SalaPayUser user, SalaPayCurrency currency);

    @Query("SELECT w FROM SalaPayWallet w WHERE w.user.id = :userId AND w.currency = :currency")
    Optional<SalaPayWallet> findWalletByUserAndCurrency(@Param("userId") Long userId, @Param("currency") SalaPayCurrency currency);

    Optional<List<SalaPayWallet>> findByUser(SalaPayUser user);
}
