package com.wagnergerald.salapay.server.server.security;

import com.wagnergerald.salapay.server.server.SalaPayConsts;
import org.springframework.context.annotation.Configuration;
import org.springframework.core.annotation.Order;
import org.springframework.security.config.annotation.authentication.builders.AuthenticationManagerBuilder;
import org.springframework.security.config.annotation.web.builders.HttpSecurity;
import org.springframework.security.config.annotation.web.configuration.WebSecurityConfigurerAdapter;


@Configuration
@Order(3)
public class Security03ConfigAPI extends WebSecurityConfigurerAdapter {

    public static final String API = "/api/1.0.0/";

    @Override
    protected void configure(HttpSecurity http) throws Exception {
        http.antMatcher(API + "**")
                .csrf().disable()
                .authorizeRequests()
                .anyRequest()
                .hasRole(SalaPayConsts.ROLE_API)
                .and()
                .httpBasic();
    }

    @Override
    protected void configure(AuthenticationManagerBuilder auth) throws Exception {
        auth.inMemoryAuthentication().withUser("api").password("{noop}api").roles(SalaPayConsts.ROLE_API);
    }

}
