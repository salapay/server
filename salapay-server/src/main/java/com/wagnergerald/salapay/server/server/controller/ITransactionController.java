package com.wagnergerald.salapay.server.server.controller;

import com.wagnergerald.salapay.server.server.model.SalaPayCurrency;
import com.wagnergerald.salapay.server.server.model.SalaPayTransaction;
import com.wagnergerald.salapay.server.server.model.SalaPayWallet;

import java.util.List;
import java.util.Optional;
import java.util.UUID;

public interface ITransactionController {

    SalaPayTransaction handleTransactionRequest(Long sourceUserId, Long targetUserId, SalaPayCurrency convertArray, Double amount);

    Long getUserIdForWallet(SalaPayWallet wallet);

    Optional<List<SalaPayTransaction>> transactionsForUser(Long userId);

    Optional<SalaPayTransaction> getTransactionById(UUID transactionId);

}
