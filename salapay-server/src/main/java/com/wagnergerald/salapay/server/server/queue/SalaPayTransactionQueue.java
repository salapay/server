package com.wagnergerald.salapay.server.server.queue;

import com.wagnergerald.salapay.server.server.SalaPayConsts;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.jms.core.JmsTemplate;
import org.springframework.stereotype.Component;

@Component
@Slf4j
public class SalaPayTransactionQueue {

    private JmsTemplate jmsTemplate;

    @Autowired
    public SalaPayTransactionQueue(JmsTemplate jmsTemplate) {
        this.jmsTemplate = jmsTemplate;
    }

    public void submitTransaction(String transactionId) {
        log.info("Putting transaction into queue with id: {}", transactionId);
        jmsTemplate.convertAndSend(SalaPayConsts.TRANSACTION_QUEUE, transactionId);
    }
}
