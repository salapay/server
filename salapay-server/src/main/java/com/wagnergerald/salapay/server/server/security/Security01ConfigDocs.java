package com.wagnergerald.salapay.server.server.security;

import com.wagnergerald.salapay.server.server.SalaPayConsts;
import org.springframework.context.annotation.Configuration;
import org.springframework.core.annotation.Order;
import org.springframework.security.config.annotation.authentication.builders.AuthenticationManagerBuilder;
import org.springframework.security.config.annotation.web.builders.HttpSecurity;
import org.springframework.security.config.annotation.web.configuration.WebSecurityConfigurerAdapter;


@Configuration
@Order(1)
public class Security01ConfigDocs extends WebSecurityConfigurerAdapter {

    public static final String SWAGGER_UI_SPRING_FOX = "/swagger-ui.html";
    public static final String SWAGGER_UI_BUNDLE = "/swagger/";

    @Override
    protected void configure(HttpSecurity http) throws Exception {
        http.requestMatchers()
                .antMatchers(SWAGGER_UI_SPRING_FOX + "**", SWAGGER_UI_BUNDLE + "**").and()
                .authorizeRequests()
                .anyRequest()
                .hasAnyRole(SalaPayConsts.ROLE_DOCS).
                and().httpBasic()
                .and()
                .logout().logoutUrl(SWAGGER_UI_BUNDLE + "/logout");
    }

    @Override
    protected void configure(AuthenticationManagerBuilder auth) throws Exception {
        auth.inMemoryAuthentication().withUser("docs").password("{noop}docs").roles(SalaPayConsts.ROLE_DOCS);
    }

}
