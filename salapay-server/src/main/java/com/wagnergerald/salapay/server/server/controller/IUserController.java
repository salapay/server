package com.wagnergerald.salapay.server.server.controller;

import com.wagnergerald.salapay.server.server.model.SalaPayUser;

import java.util.Optional;

public interface IUserController {

    SalaPayUser createUser(SalaPayUser user);

    Optional<SalaPayUser> getUserById(Long id);

}
