package com.wagnergerald.salapay.server.server.repo;

import com.wagnergerald.salapay.server.server.model.SalaPayTransaction;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.CrudRepository;
import org.springframework.data.repository.query.Param;
import org.springframework.stereotype.Repository;

import java.util.List;
import java.util.Optional;
import java.util.UUID;

@Repository
public interface ITransactionRepository extends CrudRepository<SalaPayTransaction, Long> {

    @Query("SELECT t FROM SalaPayTransaction t WHERE t.srcWallet " +
            "in (SELECT w.id FROM SalaPayWallet w WHERE w.user.id = :userId) " +
            "or t.destWallet in (SELECT w.id FROM SalaPayWallet w WHERE w.user.id = :userId)")
    Optional<List<SalaPayTransaction>> findByUserId(@Param("userId") Long userId);

    Optional<SalaPayTransaction> findByUuid(UUID uuid);

}
