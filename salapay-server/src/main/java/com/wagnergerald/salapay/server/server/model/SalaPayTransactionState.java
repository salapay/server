package com.wagnergerald.salapay.server.server.model;

public enum SalaPayTransactionState {

    REJECTED("REJECTED"),

    ACCEPTED_PENDING("ACCEPTED_PENDING"),

    ACCEPTED_PROCESSED("ACCEPTED_PROCESSED");

    private String value;

    SalaPayTransactionState(String value) {
        this.value = value;
    }

    public String getValue() {
        return value;
    }

    @Override
    public String toString() {
        return String.valueOf(value);
    }

}

